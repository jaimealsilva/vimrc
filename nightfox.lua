-- Config for EdenEast/nightfox.nvim
vim.o.background = "dark" -- or "light" for light mode

local nightfox = require('nightfox')

-- This function set the configuration of nightfox. If a value is not passed in the setup function
-- it will be taken from the default configuration above
nightfox.setup({
  options = {
    transparent = true,
    styles = {
      comments = 'italic',
      keywords = 'bold'
    }
  }
})

-- Load the configuration set above and apply the colorscheme
vim.cmd('colorscheme carbonfox')
vim.cmd('highlight Visual guibg=#555555')
vim.cmd('highlight ColorColumn guibg=#383838')
