"" hide unnecessary gui in gVim
if has("gui_running")
    set guioptions-=m  " remove menu bar
    set guioptions-=T  " remove toolbar
    set guioptions-=r  " remove right-hand scroll bar
    set guioptions-=L  " remove left-hand scroll bar
    " set default font
    set guifont=DejaVuSansMono\ Nerd\ Font\ Mono\ 10
    set background=dark
end

