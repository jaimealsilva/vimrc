local telescope = require('telescope')

telescope.load_extension('fzf')
telescope.load_extension('file_browser')

local opts = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', '<leader>ff', '<cmd>Telescope find_files<cr>', opts)
vim.api.nvim_set_keymap('n', '<C-p>', '<cmd>Telescope find_files<cr>', opts)
vim.api.nvim_set_keymap('n', '<leader>fg', '<cmd>Telescope live_grep<cr>', opts)
vim.api.nvim_set_keymap('n', '<leader>fb', '<cmd>Telescope buffers<cr>', opts)
vim.api.nvim_set_keymap('n', '<leader>fh', '<cmd>Telescope help_tags<cr>', opts)
vim.api.nvim_set_keymap('n', '<leader>fn', '<cmd>Telescope file_browser path=%:p:h<cr>', opts)

telescope.setup {
  defaults = {
    layout_strategy = 'vertical',
  }
}
