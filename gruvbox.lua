
vim.o.background = "dark" -- or "light" for light mode

require("gruvbox").setup({
  transparent_mode = true,
})

vim.cmd([[colorscheme gruvbox]])
