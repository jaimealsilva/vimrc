local hop = require('hop')
hop.setup()

local opts = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', '<leader>w', '<cmd>HopWord<cr>', opts)
