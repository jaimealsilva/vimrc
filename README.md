# Jaime's vim configuration

## Install

Clone this repo:

```bash
$ git clone <this-repo-url> ~/vimrc
```

Include these files in your ~/.vimrc and ~/.gvimrc files, and then override the
values you want to change.

### Sample ~/.vimrc

```vim
source ~/vimrc/vimrc
```

### Sample ~/.gvimrc

```vim
source ~/vimrc/gvimrc
" Change default font
set guifont=DejaVu\ Sans\ Mono\ 12
```

## Install & configure required tools

### Dein

Follow [Dein install instructions](https://github.com/Shougo/dein.vim) using ~/.vim/dein
as install path:

```bash
$ curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
$ sh ./installer.sh ~/.vim/dein
$ rm installer.sh
```

Then run gvim, ignore errors thrown because plugins are not installed yet, and run this
to install plugins:

```vim
:call dein#update()
```

### Ctags

```bash
$ sudo apt-get install ctags-exuberant
$ ln -s $HOME/vimrc/ctags ~/.ctags
```

