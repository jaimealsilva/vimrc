
" ============================================================================ "
" ===                           GUI                                        === "
" ============================================================================ "

" Set Editor Font
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    GuiFont! DejaVuSansMono\ Nerd\ Font\ Mono:h10
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Enable GUI ScrollBar
if exists(':GuiScrollBar')
    GuiScrollBar 0
endif

" ============================================================================ "
" ===                           EDITING OPTIONS                            === "
" ============================================================================ "

set mouse=a
set title
set termguicolors

" Show line number and relative numbers
set number
set relativenumber

" Show partial command
set showcmd

" Yank and paste with the system clipboard
set clipboard=unnamedplus

" Hides buffers instead of closing them
set hidden

" === TAB/Space settings === "
" indentation
set expandtab       " use spaces instead of tabs
set autoindent      " autoindent based on line above, works most of the time
set smartindent     " smarter indent for C-like languages
set shiftwidth=2    " when reading, tabs are x spaces
set softtabstop=2   " in insert mode, tabs are x spaces
set tabstop=2

" show marker at column 80
set cc=80
" do not wrap long lines by default
set nowrap
" Don't highlight current cursor line
set nocursorline

" Two lines for command line
set cmdheight=2

" always show signcolumns
set signcolumn=yes

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" === Completion Settings === "

" Don't give completion messages like 'match 1 of 2'
" or 'The only match'
" Avoid ATTENTION message when swp file exists
set shortmess+=cA

" === Search === "
" ignore case when searching
set ignorecase

" if the search string has an upper case letter in it,
" the search will be case sensitive
set smartcase
set incsearch        " find the next match as we type the search
set hlsearch         " hilight searches by default

" Automatically re-read file if a change was detected outside of vim
set autoread

" Set backups
if has('persistent_undo')
  set undofile
  set undolevels=3000
  set undoreload=10000
endif
set backupdir=~/.local/share/nvim/backup " Don't put backups in current dir
set nobackup " Better disable
set noswapfile

" Allow :find to look in all project
set path+=**

" turn on syntax highlighting
syntax on

" set unix line endings
set fileformat=unix
" when reading files try unix line endings then dos, also use unix for new
" buffers
set fileformats=unix,dos

" save up to 100 marks, enable capital marks
set viminfo='100,f1

" screen will not be redrawn while running macros, registers or other
" non-typed comments
set lazyredraw

" Allow more time for loading syntax on large files
set redrawtime=10000

" suggestion for normal mode commands
set wildmode=list:longest

" keep the cursor visible within 3 lines when scrolling
set scrolloff=3
set sidescrolloff=8

" Move swap files out of projects
set dir=$HOME/.vim_tmp/swap
if !isdirectory(&dir) | call mkdir(&dir, 'p', 0700) | endif

set list
set listchars=tab:▸\ ,trail:∙

" On file types...
"   .md files are markdown files
autocmd BufNewFile,BufRead *.md setlocal ft=markdown
autocmd BufNewFile,BufRead *.md setlocal syntax=markdown
"   .twig files use html syntax
autocmd BufNewFile,BufRead *.twig setlocal ft=html
autocmd BufNewFile,BufRead *.twig setlocal syntax=html
"   .less files use less syntax
autocmd BufNewFile,BufRead *.less setlocal ft=less
autocmd BufNewFile,BufRead *.less setlocal syntax=less
"   .jade files use jade syntax
autocmd BufNewFile,BufRead *.jade setlocal ft=jade
autocmd BufNewFile,BufRead *.jade setlocal syntax=jade
"   PHP files
autocmd BufNewFile,BufRead *.php setlocal ft=php
autocmd BufNewFile,BufRead *.php setlocal syntax=php
autocmd BufNewFile,BufRead *.module setlocal ft=php
autocmd BufNewFile,BufRead *.module setlocal syntax=php
autocmd BufNewFile,BufRead *.inc setlocal ft=php
autocmd BufNewFile,BufRead *.inc setlocal syntax=php
autocmd BufNewFile,BufRead *.install setlocal ft=php
autocmd BufNewFile,BufRead *.install setlocal syntax=php
autocmd BufNewFile,BufRead *.test setlocal ft=php
autocmd BufNewFile,BufRead *.test setlocal syntax=php
autocmd BufNewFile,BufRead *.profile setlocal ft=php
autocmd BufNewFile,BufRead *.profile setlocal syntax=php
autocmd BufNewFile,BufRead *.action setlocal ft=php
autocmd BufNewFile,BufRead *.action setlocal syntax=php
"   TypeScript
autocmd BufNewFile,BufRead *.ts setlocal ft=typescript
autocmd BufNewFile,BufRead *.ts setlocal syntax=typescript

"============================================================
"                 KEYMAPS
"============================================================

" Remap leader key to space
nnoremap <SPACE> <Nop>
let g:mapleader = ' '

" use leader-space to remove search higlight
nnoremap <leader><space> :nohlsearch<cr>

" allow Tab and Shift+Tab to
" tab  selection in visual mode
vmap <Tab> >gv
vmap <S-Tab> <gv

" Quicker switching between windows
nmap <silent> <C-h> <C-w>h
nmap <silent> <C-j> <C-w>j
nmap <silent> <C-k> <C-w>k
nmap <silent> <C-l> <C-w>l

" Make gf create non-existent files
nmap gf :edit <cfile><cr>

" ============================================================================ "
" ===                           PLUGINS                                    === "
" ============================================================================ "

let g:vimspector_enable_mappings = 'HUMAN'

" Use vim-plug https://github.com/junegunn/vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
call plug#begin(data_dir . '/plugins')
  " Color Schemes
  Plug 'EdenEast/nightfox.nvim'
  Plug 'ellisonleao/gruvbox.nvim'
  " end Color Schemes
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-surround'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'tpope/vim-fugitive'
  Plug 'jiangmiao/auto-pairs'
  Plug 'puremourning/vimspector'
  Plug 'mattn/emmet-vim'
  Plug 'nvim-lualine/lualine.nvim'
  Plug 'kyazdani42/nvim-web-devicons'
  " Plug 'github/copilot.vim'
  " Plug 'TabbyML/vim-tabby'
  " Telescope
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
  Plug 'nvim-telescope/telescope-file-browser.nvim'
  " end Telescope
  " LSP
  Plug 'neovim/nvim-lspconfig'
  Plug 'hrsh7th/cmp-nvim-lsp'
  Plug 'hrsh7th/cmp-buffer'
  Plug 'hrsh7th/cmp-path'
  Plug 'hrsh7th/cmp-cmdline'
  Plug 'hrsh7th/nvim-cmp'
  Plug 'L3MON4D3/LuaSnip'
  Plug 'saadparwaiz1/cmp_luasnip'
  " end LSP
  " Treesitter
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  " end Treesitter
  Plug 'phaazon/hop.nvim'
  Plug 'numkil/ag.nvim'
  " Go
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
  " end Go
  " Prettier
  Plug 'prettier/vim-prettier', { 'do': 'yarn install --frozen-lockfile --production' }
call plug#end()

" Plugin configuration is put on external files

luafile ~/vimrc/nightfox.lua
"luafile ~/vimrc/gruvbox.lua
luafile ~/vimrc/treesitter.lua
source ~/vimrc/fugitive.vim
luafile ~/vimrc/telescope.lua
luafile ~/vimrc/lualine.lua
luafile ~/vimrc/lsp.lua
luafile ~/vimrc/hop.lua
source ~/vimrc/vimspector.vim
" source ~/vimrc/copilot.vim
" source ~/vimrc/tabby.vim
source ~/vimrc/go.vim
