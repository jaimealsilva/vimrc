require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    disable = {},
  },
  indent = {
    enable = false,
    disable = {},
  },
  ensure_installed = {
    "tsx",
    "bash",
    "php",
    "json",
    "yaml",
    "html",
    "scss",
    "lua",
    "javascript",
    "typescript",
    "go"
  },
}
