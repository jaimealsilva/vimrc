imap <silent><script><expr> <C-J> copilot#Accept("\<CR>")
let g:copilot_no_tab_map = v:true
imap <M-l> <Plug>(copilot-accept-word)
imap <M-C-l> <Plug>(copilot-accept-line)
